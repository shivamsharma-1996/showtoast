package shivam.com.mylibrary;

import android.content.Context;
import android.widget.Toast;

public class ShowToast {

    public static void showToast(Context context,String body)
    {
        Toast.makeText(context, body, Toast.LENGTH_SHORT).show();
    }
}
